﻿/*
KrampusMod Common
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using UnityEngine;

namespace KrampusModCommon
{
	internal static class BepInExHelpers
	{
		public static ObjectDB FindObjectDB()
		{
			GameObject gameMainObj = GameObject.Find("_GameMain");
			if (gameMainObj != null)
			{
				ObjectDB objectDB = gameMainObj.GetComponent<ObjectDB>();
				if (objectDB != null)
				{
					return objectDB;
				}
			}
			return null;
		}
	}
}
