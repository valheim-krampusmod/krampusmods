﻿/*
KrampusMod Common
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using System;
using System.Collections.Generic;

namespace KrampusModCommon
{
	public class BepInPluginTemplate : BaseUnityPlugin
	{
		// Config - Debug
		public static ConfigEntry<bool> enableLogging;
		public static ConfigEntry<bool> enableTranspilerLogging;

		protected BepInPlugin BepInAttr { get; set; }
		protected static Harmony HarmonyInst { get; set; }
		protected static BepInPluginTemplate ModInst { get; set; }
		protected static new ManualLogSource Logger { get; set; }

		public enum LogMessageType
		{
			LogInfo,
			LogMessage,
			LogDebug,
			LogWarning,
			LogError,
			LogFatal
		}

		protected virtual void Awake()
		{
			ModInst = this;
			BepInAttr = (BepInPlugin)Attribute.GetCustomAttribute(GetType(), typeof(BepInPlugin));
			Logger = BepInEx.Logging.Logger.CreateLogSource(BepInAttr.Name);
			HarmonyInst = new Harmony(BepInAttr.GUID);
			enableLogging = Config.Bind("Debug", "Logging", false, "");
			enableTranspilerLogging = Config.Bind("Debug", "TranspilerLogging", false, "");
			DebugLog($"Loading {BepInAttr.GUID}...");
		}

		protected virtual void OnDestroy()
		{
			DebugLog($"Unloading {BepInAttr.GUID}...");
			HarmonyInst?.UnpatchSelf();
			BepInEx.Logging.Logger.Sources.Remove(Logger);
		}

		public static void DebugLog(object message, LogMessageType msgType = LogMessageType.LogInfo, bool transpilerlogs = false)
		{
			if (enableLogging != null && enableLogging.Value)
			{
				if (transpilerlogs && enableTranspilerLogging != null && !enableTranspilerLogging.Value)
				{
					return;
				}

				switch (msgType)
				{
					case LogMessageType.LogMessage:
						Logger.LogMessage(message);
						break;
					case LogMessageType.LogDebug:
						Logger.LogDebug(message);
						break;
					case LogMessageType.LogWarning:
						Logger.LogWarning(message);
						break;
					case LogMessageType.LogError:
						Logger.LogError(message);
						break;
					case LogMessageType.LogFatal:
						Logger.LogFatal(message);
						break;
					default:
						Logger.LogInfo(message);
						break;
				}

			}
		}

		public static void DebugTranspilerLog(object message, LogMessageType msgType = LogMessageType.LogInfo) => DebugLog(message, msgType, true);

		public static void PrintOutInstructions(List<CodeInstruction> instructions)
		{
			DebugTranspilerLog("");
			DebugTranspilerLog($"!!! MODIFIED INSTRUCTIONS - {instructions.Count} !!!");
			DebugTranspilerLog("");

			for (int i = 0; i < instructions.Count; i++)
			{
				CodeInstruction instr = instructions[i];
				DebugTranspilerLog($"{i} {instr}");
			}
		}

	}
}
