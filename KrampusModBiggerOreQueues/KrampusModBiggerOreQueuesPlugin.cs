﻿/*
KrampusMod Bigger Ore Queues
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using BepInEx;
using BepInEx.Configuration;
using KrampusModCommon;
using ServerSync;

namespace KrampusModBiggerOreQueues
{
	[BepInPlugin(modGuid, modName, modVersion)]
	public class KrampusModBiggerOreQueuesPlugin : BepInPluginTemplate
	{
		public const string modVersion = "1.3.0";
		public const string modMinVersion = "1.3.0"; // Minimum version needed for ConfigSync.
		public const string modName = "KrampusModBiggerOreQueues";
		private const string modGuid = "krampusmod.BiggerOreQueues";

		public enum Toggle { On = 1, Off = 0 };

		public static ConfigEntry<Toggle> lockConfiguration;
		public static ConfigEntry<int> maxKilnFuelCapacity;
		public static ConfigEntry<int> maxSmelterFuelCapacity;
		public static ConfigEntry<int> maxSmelterOreCapacity;
		public static ConfigEntry<bool> blastFurnaceSmeltsAll;

		public static ConfigSync configSync;

		private void SetupConfiguration()
		{
			configSync = new ConfigSync(modGuid)
			{
				DisplayName = modName,
				CurrentVersion = modVersion,
				MinimumRequiredVersion = modMinVersion
			};
			lockConfiguration = AddConfig("1 - ServerSync", "LockConfiguration", Toggle.On, "If On, then configuration is locked and can be changed by server admins only");
			configSync.AddLockingConfigEntry(lockConfiguration);
			maxKilnFuelCapacity = AddConfig("2 - General", "MaxKilnFuelCapacity", 25, "Adjusts the maximum amount that the kiln can hold for producing coal from wood");
			maxSmelterFuelCapacity = AddConfig("2 - General", "MaxSmelterFuelCapacity", 20, "Adjust the maximum number of fuel pieces that can be queued up in the smelter at once");
			maxSmelterOreCapacity = AddConfig("2 - General", "MaxSmelterOreCapacity", 10, "Adjust the maximum number of raw ores that can be queued up in the smelter at once");
			blastFurnaceSmeltsAll = AddConfig("2 - General", "BlastFurnaceSmeltsAll", false, "Changes the blast furnace to be able to smelt all known ores");
		}


		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, ConfigDescription description, bool synchronizedSetting = true)
		{
			ConfigDescription _description = new ConfigDescription(description.Description + (synchronizedSetting ? " [Synced with Server]" : " [Not Synced with Server]"), description.AcceptableValues, description.Tags);
			ConfigEntry<T> configEntry = Config.Bind(group, name, value, _description);
			SyncedConfigEntry<T> syncedConfigEntry = configSync.AddConfigEntry(configEntry);
			syncedConfigEntry.SynchronizedConfig = synchronizedSetting;
			return configEntry;
		}

		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, string description, bool synchronizedSetting = true) => AddConfig(group, name, value, new ConfigDescription(description), synchronizedSetting);

		protected override void Awake()
		{
			base.Awake();
			SetupConfiguration();
			HarmonyInst.PatchAll(typeof(BiggerOreQueuePatches));

			// Refresh values when reloading the mod with F6.
			ObjectDB objectDB = BepInExHelpers.FindObjectDB();
			if (objectDB != null)
			{
				DebugLog($"ObjectDB is not null, instance ID is {objectDB.GetInstanceID()}");
			}
		}
	}
}
