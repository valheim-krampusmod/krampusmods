# KrampusMod: Bigger Ore Queues

Mod for Valheim that allows kilns and smelters to have bigger queues.
The amount is configurable.
Additionally, the blast furnace can be turned into a super-smelter, smelting all-the-things.
