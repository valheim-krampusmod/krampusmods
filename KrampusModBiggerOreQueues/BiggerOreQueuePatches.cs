﻿/*
KrampusMod Bigger Ore Queues
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using HarmonyLib;
using KrampusModCommon;
using System.Collections.Generic;
using System.Linq;

namespace KrampusModBiggerOreQueues
{
	internal static class BiggerOreQueuePatches
	{
		[HarmonyPatch(typeof(Smelter), "UpdateSmelter")]
		[HarmonyPostfix]
		private static void UpdateSmelter_Postfix(Smelter __instance, ZNetView ___m_nview, ref int ___m_maxFuel, ref int ___m_maxOre)
		{
			if (!Player.m_localPlayer || ___m_nview == null)
			{
				return;
			}

			if (__instance.m_name.Equals("$piece_charcoalkiln"))
			{
				int newMaxKilnFuelCapacity = KrampusModBiggerOreQueuesPlugin.maxKilnFuelCapacity.Value;
				___m_maxOre = newMaxKilnFuelCapacity;
				BepInPluginTemplate.DebugLog($"Maximum fuel capacity for {__instance.m_name} is now {__instance.m_maxOre}");
			}
			else if (__instance.m_name.Equals("$piece_smelter") || __instance.m_name.Equals("$piece_blastfurnace"))
			{
				int newMaxSmelterFuelCapacity = KrampusModBiggerOreQueuesPlugin.maxSmelterFuelCapacity.Value;
				int newMaxSmelterOreCapacity = KrampusModBiggerOreQueuesPlugin.maxSmelterOreCapacity.Value;
				___m_maxFuel = newMaxSmelterFuelCapacity;
				___m_maxOre = newMaxSmelterOreCapacity;
				BepInPluginTemplate.DebugLog($"Maximum fuel capacity for {__instance.m_name} is now {__instance.m_maxFuel}");
				BepInPluginTemplate.DebugLog($"Maximum ore capacity for {__instance.m_name} is now {__instance.m_maxOre}");
			}
			else
			{
				/*
				 * Other possible types:
				 * $piece_spinningwheel
				 * $piece_windmill
				 */
				BepInPluginTemplate.DebugLog($"Not changing capacity values for {__instance.m_name} because I do not support it");
			}
		}

		// Additional ore-types that are blast furnace smeltable.
		static Dictionary<string, ItemDrop> extraBlastFurnaceMetals = new Dictionary<string, ItemDrop>
		{
			{ "$item_copperore", null },
			{ "$item_copper", null },
			{ "$item_ironscrap", null },
			{ "$item_iron", null },
			{ "$item_tinore", null },
			{ "$item_tin", null },
			{ "$item_silverore", null },
			{ "$item_silver", null },
			{ "$item_copperscrap", null }
		};

		public static Dictionary<string, ItemDrop> ExtraBlastFurnaceMetals { get => extraBlastFurnaceMetals; set => extraBlastFurnaceMetals = value; }

		[HarmonyPatch(typeof(Smelter), "Awake")]
		[HarmonyPostfix]
		private static void AwakeSmelter_Postfix(ref Smelter __instance)
		{
			if (!KrampusModBiggerOreQueuesPlugin.blastFurnaceSmeltsAll.Value || !__instance.m_name.Equals("$piece_blastfurnace"))
			{
				return;
			}

			ObjectDB objDBInstance = ObjectDB.instance;
			List<ItemDrop> materials = objDBInstance.GetAllItems(ItemDrop.ItemData.ItemType.Material, "");

			// Create references to each material for the name given in the smeltable dictionary.
			foreach (ItemDrop material in materials)
			{
				if (ExtraBlastFurnaceMetals.Keys.Contains(material.m_itemData.m_shared.m_name))
				{
					BepInPluginTemplate.DebugLog($"Adding {material.m_itemData.m_shared.m_name} to list of materials");
					ExtraBlastFurnaceMetals[material.m_itemData.m_shared.m_name] = material;
				}
			}

			// Defines additional conversions that the blast furnace will perform when allowed materials are added.
			// Example: create a conversion from $item_silverore -> $item_silver.
			List<Smelter.ItemConversion> conversions = new List<Smelter.ItemConversion>()
			{
				new Smelter.ItemConversion{ m_from = ExtraBlastFurnaceMetals["$item_copperore"], m_to = ExtraBlastFurnaceMetals["$item_copper"]},
				new Smelter.ItemConversion{ m_from = ExtraBlastFurnaceMetals["$item_tinore"], m_to = ExtraBlastFurnaceMetals["$item_tin"]},
				new Smelter.ItemConversion{ m_from = ExtraBlastFurnaceMetals["$item_ironscrap"], m_to = ExtraBlastFurnaceMetals["$item_iron"]},
				new Smelter.ItemConversion{ m_from = ExtraBlastFurnaceMetals["$item_silverore"], m_to = ExtraBlastFurnaceMetals["$item_silver"]},
				new Smelter.ItemConversion{ m_from = ExtraBlastFurnaceMetals["$item_copperscrap"], m_to = ExtraBlastFurnaceMetals["$item_copper"]}

			};

			// Adds each additional conversion type to the instance's ItemConversion collection.
			// We add instead of replace in case the base game expands the conversion we don't accidentally wipe out
			// new convertibles (e.g. Flametal in the Ashlands when it became available).
			foreach (Smelter.ItemConversion conversion in conversions)
			{
				__instance.m_conversion.Add(conversion);
			}
		}
	}
}
