/*
KrampusMod Carry Weight Modifier
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using BepInEx;
using BepInEx.Configuration;
using KrampusModCommon;
using ServerSync;

namespace KrampusModCarryWeightModifier
{

	[BepInPlugin(modGuid, modName, modVersion)]
	public class KrampusModCarryWeightModifierPlugin : BepInPluginTemplate
	{
		public const string modVersion = "1.2.1";
		public const string modMinVersion = "1.2.1"; // Minimum version needed for ConfigSync.
		public const string modName = "KrampusModCarryWeightModifier";
		private const string modGuid = "krampusmod.CarryWeightModifier";
		public const string modAuthor = "KrazyKrampus";

		public enum Toggle { On = 1, Off = 0 };

		public static ConfigEntry<Toggle> lockConfiguration;
		public static ConfigEntry<int> maxCarryWeight;
		public static ConfigEntry<int> megingjordBuffWeight;

		public static ConfigSync configSync;

		private void SetupConfiguration()
		{
			configSync = new ConfigSync(modGuid)
			{
				DisplayName = modName,
				CurrentVersion = modVersion,
				MinimumRequiredVersion = modMinVersion
			};
			lockConfiguration = AddConfig("1 - ServerSync", "LockConfiguration", Toggle.On, "If On, then configuration is locked and can be changed by server admins only");
			configSync.AddLockingConfigEntry(lockConfiguration);
			maxCarryWeight = AddConfig("2 - General", "MaxCarryWeight", 300, new ConfigDescription("Adjusts the carry weight to the configured value from the default value of 300"));
			megingjordBuffWeight = AddConfig("2 - General", "MegingjordBuffWeight", 150, new ConfigDescription("Adjusts how much extra weight the Megingjord buff gives from the vanilla 150 to the value configured here"));
		}

		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, ConfigDescription description, bool synchronizedSetting = true)
		{
			ConfigDescription _description = new ConfigDescription(description.Description + (synchronizedSetting ? " [Synced with Server]" : " [Not Synced with Server]"), description.AcceptableValues, description.Tags);
			ConfigEntry<T> configEntry = Config.Bind(group, name, value, _description);
			SyncedConfigEntry<T> syncedConfigEntry = configSync.AddConfigEntry(configEntry);
			syncedConfigEntry.SynchronizedConfig = synchronizedSetting;
			return configEntry;
		}

		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, string description, bool synchronizedSetting = true) => AddConfig(group, name, value, new ConfigDescription(description), synchronizedSetting);

		protected override void Awake()
		{
			base.Awake();
			SetupConfiguration();
			HarmonyInst.PatchAll(typeof(CarryWeightPatches));

			// Refresh the values when reloading the mod with F6.
			ObjectDB objectDB = BepInExHelpers.FindObjectDB();
			if (objectDB != null)
			{
				DebugLog($"ObjectDB is not null, instance ID is {objectDB.GetInstanceID()}");
			}
		}
	}
}
