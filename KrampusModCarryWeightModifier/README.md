# KrampusMod: Carry Weight Modifier

Mod for Valheim that allows the player's carry weight to be modified.
The amount is configurable.
