﻿/*
KrampusMod Carry Weight Modifier
Copyright (c) 2022 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using HarmonyLib;
using KrampusModCommon;

namespace KrampusModCarryWeightModifier
{
	internal static class CarryWeightPatches
	{
		private static float maxCarryWeight = 300f;

		[HarmonyPatch(typeof(Player), "GetMaxCarryWeight")]
		[HarmonyPostfix]
		private static void GetMaxCarryWeight_Postfix(ref float __result)
		{
			float carryWeight = __result;
			float newCarryWeight = KrampusModCarryWeightModifierPlugin.maxCarryWeight.Value;
			float megingjordWeight = KrampusModCarryWeightModifierPlugin.megingjordBuffWeight.Value;
			bool Megingjord = false;

			if (carryWeight > 300f)
			{
				Megingjord = true;
				carryWeight -= 150f;
			}

			if (Megingjord)
			{
				carryWeight = newCarryWeight + megingjordWeight;
			}
			else
			{
				carryWeight = newCarryWeight;
			}

			if (maxCarryWeight == 300f)
			{
				BepInPluginTemplate.DebugLog($"Changing default carry weight {maxCarryWeight} to {carryWeight}; Megingjord was {Megingjord}");
				maxCarryWeight = carryWeight;
			}

			__result = carryWeight;
		}
	}
}
