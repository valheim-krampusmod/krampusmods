﻿/*
KrampusMod Better Food
Copyright (c) 2024 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Reflection;
using BepInEx;
using BepInEx.Configuration;
using JetBrains.Annotations;
using KrampusModBetterFood.Patches;
using KrampusModCommon;
using ServerSync;

namespace KrampusModBetterFood
{
	[BepInPlugin(modGuid, modName, modVersion)]
	public class KrampusModBetterFood : BepInPluginTemplate
	{
		public const string modVersion = "1.0.0";
		public const string modMinVersion = "1.0.0"; // Minimum version needed for ConfigSync.
		public const string modName = "KrampusModBetterFood";
		private const string modGuid = "krampusmod.BetterFood";
		public const string modAuthor = "KrazyKrampus";

		public enum Toggle { On = 1, Off = 0 };

		public static ConfigEntry<Toggle> lockConfiguration;
		public static ConfigEntry<float> durationMultiplier;

		public static ConfigSync configSync;

		private void SetupConfiguration()
		{
			configSync = new ConfigSync(modGuid)
			{
				DisplayName = modName,
				CurrentVersion = modVersion,
				MinimumRequiredVersion = modMinVersion
			};
			lockConfiguration = AddConfig("1 - ServerSync", "LockConfiguration", Toggle.On, "If On, then configuration is locked and can be changed by server admins only");
			durationMultiplier = AddConfig("2 - General", "DurationMultiplier", 1f, new ConfigDescription("Adjusts what we multiply the food timers by on all foods", new AcceptableValueRange<float>(0.1f, 10f)));
		}

		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, ConfigDescription description, bool synchronizedSetting = true)
		{
			ConfigDescription _description = new ConfigDescription(description.Description + (synchronizedSetting ? " [Synced with Server]" : " [Not Synced with Server]"), description.AcceptableValues, description.Tags);
			ConfigEntry<T> configEntry = Config.Bind(group, name, value, _description);
			SyncedConfigEntry<T> syncedConfigEntry = configSync.AddConfigEntry(configEntry);
			syncedConfigEntry.SynchronizedConfig = synchronizedSetting;
			return configEntry;
		}

		private ConfigEntry<T> AddConfig<T>(string group, string name, T value, string description, bool synchronizedSetting = true) => AddConfig(group, name, value, new ConfigDescription(description), synchronizedSetting);

		[UsedImplicitly]
		protected override void Awake()
		{
			base.Awake();
			SetupConfiguration();
			HarmonyInst.PatchAll(Assembly.GetExecutingAssembly());

			// Refresh the values when reloading the mod with F6.
			ObjectDB objectDB = BepInExHelpers.FindObjectDB();
			if (objectDB != null)
			{
				DebugLog($"ObjectDB is not null, instance ID is {objectDB.GetInstanceID()}");
			}
		}
	}
}
