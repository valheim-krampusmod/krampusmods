﻿/*
KrampusMod Better Food
Copyright (c) 2024 Thomas J Farvour

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using HarmonyLib;
using JetBrains.Annotations;

namespace KrampusModBetterFood.Patches
{
	[HarmonyPatch]
	public class PlayerPatch
	{
		[HarmonyPatch(typeof(Player), "EatFood")]
		public static class EatFoodPatch
		{
			private static readonly FieldInfo _fieldInfo = AccessTools.Field(typeof(ItemDrop.ItemData.SharedData), "m_foodBurnTime");
			private static readonly MethodInfo _methodInfo = AccessTools.Method(typeof(EatFoodPatch), "GetMultipliedFoodDuration", null, null);

			/// <summary>
			/// Injects a duration modifier whenever we eat food.
			/// </summary>
			/// <param name="instructions"></param>
			/// <returns></returns>
			[HarmonyTranspiler]
			public static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
			{
				//IL_0034: Unknown result type (might be due to invalid IL or missing references)
				//IL_003e: Expected O, but got Unknown
				List<CodeInstruction> list = instructions.ToList();
				int num = list.Count;
				for (int i = 0; i < num; i++)
				{
					if (CodeInstructionExtensions.LoadsField(list[i], _fieldInfo, false))
					{
						list.Insert(i + 1, new CodeInstruction(OpCodes.Call, _methodInfo));
						num++; // Skip over original instruction.
					}
				}
				return list.AsEnumerable();
			}

			[UsedImplicitly]
			private static float GetMultipliedFoodDuration(float m_foodBurnTime) => m_foodBurnTime * KrampusModBetterFood.durationMultiplier.Value;
		}

	}
}
